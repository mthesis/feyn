import numpy as np
import matplotlib.pyplot as plt

import sys
fix=True
if len(sys.argv)>1:
  fix=False



f=np.load("eval.npz")

q=np.load("fids.npz")["q"]


x,p=f["x"],f["p"]


x=x[:,:,:p.shape[-1]]


l=(x-p)**2

l=np.mean(l,axis=(1,2))
l=np.sqrt(l)


ln=[]
qn=[]

for i,ll in enumerate(l):
  if q[i][0]=="4" or fix:
    ln.append(ll)
    qn.append(q[i])

c=0

for ll,qq in zip(ln,qn):
  if ll>0.1:
    c+=1
    print(qq,ll)
  else:
    print("      ",qq,ll)

print(c,len(ln),c/len(ln))
print("maxima",qn[np.argmax(ln)])
print("minima",qn[np.argmin(ln)])

plt.hist(ln,bins=20)

plt.show()


