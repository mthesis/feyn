import numpy as np
import matplotlib.pyplot as plt

import sys
fix=True
if len(sys.argv)>1:
  fix=False



f=np.load("eval.npz")

q=np.load("fids.npz")["q"]


x,p=f["x"],f["p"]


x=x[:,:,:p.shape[-1]]


l=(x-p)**2

l=np.mean(l,axis=(1,2))
l=np.sqrt(l)


ln=[]
qn=[]
xn=[]


for i,ll in enumerate(l):
  if q[i][0]=="4" or fix:
    ln.append(ll)
    qn.append(q[i])
    xn.append(x[i])


def getn(q):
  return np.sum([x[-1] for x in q])

n=[getn(q) for q in x]


plt.plot(n,ln,"o",alpha=0.5)


plt.show()








